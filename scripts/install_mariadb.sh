#!/bin/bash

MARIADB=mariadb
NAMESPACE=notes-app
DB_ROOT_PWD=y33tboi

kubectl get namespace $NAMESPACE
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "Error: Could not find namespace '$NAMESPACE', will try to create it"
    kubectl create namespace $NAMESPACE
    retVal=$?
    if [ $retVal -ne 0 ]; then
        echo "Error: Could not create namespace '$NAMESPACE'"
        exit $retVal
    fi
fi

helm get all $MARIADB -n $NAMESPACE
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "Error: Could not find release '$MARIADB' in namespace '$NAMESPACE', will try to install"
    helm upgrade --namespace $NAMESPACE --install $MARIADB mariadb --repo https://charts.bitnami.com/bitnami --set auth.rootPassword=$DB_ROOT_PWD --set primary.service.clusterIP=None
    retVal=$?
    if [ $retVal -ne 0 ]; then
        echo "Error: Could not install '$MARIADB' in namespace '$NAMESPACE', Checking Logs:\n"
        kubectl logs $(kubectl get pods -n $NAMESPACE | grep $MARIADB | awk '{print $1}')
        exit $retVal
    fi
fi

exit 0

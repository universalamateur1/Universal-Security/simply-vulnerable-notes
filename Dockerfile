FROM python:3.10-bullseye

RUN apt-get update -y
RUN apt install gcc git openssl -y

RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
RUN curl -LsS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash
RUN apt install libmariadb3 libmariadb-dev mariadb-client sqlite3 libsqlite3-dev -y

RUN pip3 install --upgrade pip
RUN pip3 install packaging

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app

EXPOSE 3306/tcp
EXPOSE 5000/tcp
ENTRYPOINT [ "python" ]
CMD [ "run.py" ]

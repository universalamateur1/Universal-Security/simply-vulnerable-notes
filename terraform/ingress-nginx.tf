provider "helm" {
  kubernetes {
    host                   = "https://${google_container_cluster.primary.endpoint}"
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
  }
}

resource "helm_release" "ingress-nginx" {
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  create_namespace = true
  namespace        = var.ingress_namespace
  timeout          = 1500

  set {
    name  = "controller.config.enable-real-ip"
    value = "\"true\""
  }

  set {
    name  = "controller.config.use-forwarded-headers"
    value = "\"true\""
  }

  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "\"Local\""
  }

  depends_on = [
    google_container_cluster.primary
  ]
}